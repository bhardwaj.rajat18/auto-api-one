"""Constants to avoid hard coding, enable code reuse and to minimise modification in tests if something changes"""
RESOURCE_TYPE_PERSON = 'person'
RESOURCE_TYPE_PLACE = 'place'
MAX_NUM_RECORDS_SUPPORTED = 100
DEFAULT_EXPECTED_RECORDS_PER_PAGE = 10
CULTURE_DUTCH = "Dutch"
GENDER_FEMALE = "female"
GENDER_MALE = "male"

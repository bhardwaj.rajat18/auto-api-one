import time


class LoggerSettings:
    """Class to set logger related settings"""
    LOGGER_PATH = "/../logs/"
    LOGGER_NAME = 'output'
    LOG_FILE_NAME = "output-" + time.strftime('%d-%m-%Y_%H-%M-%S') + ".log"
    LOGGER = ''

from settings.common_settings import CommonSettings
from api.base import APIBase


class RequestHelper:
    """Class to construct required payloads and call api caller method from base with the same"""

    # Create object for base
    api_base = APIBase(base_url=CommonSettings.ENV['API']['ENDPOINT'])

    def api_get_query(self, resource_type, **kwargs):
        """
        This function to construct payload for harvard museum apis or any api that supports harvard museum api styles
        :param resource_type: Request entity. Ex: Person, Place, Object, Gallery etc.
        :type resource_type:str
        :param kwargs:keyworded arguments which should contain all the query parameters needed for the request payload
        :type kwargs:
        :return: json response
        :rtype: dict
        """
        # Update request parameters with API key
        query_params = {
            'apikey': CommonSettings.ENV['API']['KEY']
        }

        # Update query parameters with all the parameters sent by the caller
        for k, v in kwargs.items():
            if k == 'query':
                query_params['q'] = kwargs['query']
            else:
                query_params[k] = v

        # Call and return result from base api caller method
        return self.api_base.call_api_get(resource_type=resource_type, params=query_params,
                                          api_timeout=CommonSettings.ENV['API']['TIMEOUT'])


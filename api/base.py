import requests
from settings.logger import LoggerSettings
import assertions


class APIBase:
    """Class to configure base url and make the API calls"""

    logger = LoggerSettings.LOGGER

    def __init__(self, base_url):
        """
        Constructor where we define the base url. For every new Base URL, caller needs a new object of this class.
        :param base_url: The base http url
        :type base_url: str
        """
        self.base_url = base_url

    def call_api_get(self, resource_type, params, api_timeout, expected_status_code=200):
        """
        A simple function which uses 'requests' module to call an API with GET HTTP method
        :param resource_type: The type of entity defined API documentation. Person, Place etc.
        :type resource_type: str
        :param params: Parameters which will form the query for get API. Ex: BASE_URL/person?size=10, here, size is a
                       param
        :type params: dict
        :param api_timeout: Timeout till which the API should wait for the response
        :type api_timeout: int
        :param expected_status_code: Expected http status code from the response. 2XX, 4XX etc.
        :type expected_status_code: int
        :return: json response
        :rtype: json
        """
        resp = requests.get(url=self.base_url + resource_type, timeout=api_timeout, params=params)
        self.logger.info("Invoking {0}".format(self.base_url + resource_type))
        self.logger.info("Request Params: {0}".format(resp.url.split("&")[1:]))
        self.logger.info("Status Code: {0}, Response Time: {1}s".format(resp.status_code, resp.elapsed.total_seconds()))
        self.logger.info("JSON Response: {0}".format(resp.json()))
        assertions.verify_status_code(resp=resp, status_code=expected_status_code)
        return resp.json()

    def call_api_post(self, post_data, api_timeout, expected_status_code=200):
        pass

# Auto api one

## Context

From the official API documentation, "The Harvard Art Museums API is a REST-style service designed for developers who
wish to explore and integrate the museums’ collections in their projects. The API provides direct access to the data
that powers the museums' website and many other aspects of the museums". More details can be
found [here](https://github.com/harvardartmuseums/api-docs).

## Objective

To create a project to integrate Harvard Museums (HM) API and test it's functionalities.

## Scope

- Currently, this project validates **Person** resource type for HM API. Other available resource types can be
  found [here](https://github.com/harvardartmuseums/api-docs#resources-that-are-available)
- The project has 5 test cases covered, for now.
- More resource types and tests can be integrated with the project by following the guidelines
  mentioned [here](https://gitlab.com/bhardwaj.rajat18/auto-api-one/-/wikis/Wiki#developing-new-test-cases)

## Description

The project supports automated API validation for Harvard Museums (HM) API. Some features of the project:

- Python3.x
- Pytest framework
- Generic functions which can be used to test any HM API
- Logging
- YAML config

More details can be found on [wiki](https://gitlab.com/bhardwaj.rajat18/auto-api-one/-/wikis/Wiki)

## Installation

##### Prerequisites

- Python3.x should be installed
- pip should be installed. Installation details can be found [here](https://pip.pypa.io/en/stable/installation/)

```
Tip: To check if python3 is installed, on the terminal please run: python3 --version
```

##### Setting up the project

- ```pip install virtualenv```
- ```virtualenv venv``` (installs a virtual environment named venv in the current path)
- ```git clone https://gitlab.com/bhardwaj.rajat18/auto-api-one.git```
- ```cd auto-api-one```
- ```source <path_to_venv>/bin/activate```  
- ```pip install requirements.pip```

```
Note: You can append "python3 -m" before any python command above if there are multiple python versions available or 
any of the commands doesn't work as expected, given that python3 and pip are installed.
For example, run python3 -m pip install virtualenv to make sure virtualenv compatible with Python3.x is installed.
```

## Usage

- Activate virtual environment, if not  already done and go to "_tests/_" directory
   ```
  source <path_to_venv>/bin/activate
  cd tests/
   ```
#### Examples

- To run all the test cases in tests/
    ```
    pytest -sv --env production
    ```
- To run a particular test case
    ```
    pytest -sv test_resource_person.py::TestResourcePerson::test_person_records_length --env production
    OR
    pytest -sv -k test_person_records_length --env production
    ```
- To run tests with markers ("sanity", "negative", "regression")
    ```
    pytest -sv -m sanity --env production
    ```
- To run tests and generate html report (Append "--html=<filename.html> --self-contained-html" to any of the above
  commands)
    ```
    pytest -sv --env production --html=report.html --self-contained-html
    ```

## Example code snippets

#### Config

```yaml
API:
  ENDPOINT: "https://api.harvardartmuseums.org/"
```

#### Test case

```python
def test_person_records_length(self):
      json_response = self.request_util.api_get_query(resource_type=RESOURCE_TYPE_PERSON, query="culture:Dutch")
      assertions.verify_response_has_attributes(response=json_response, expected_attributes=['info',
                                                                                             'totalrecordsperquery',
                                                                                             'records'])
      expected_length = json_response['info']['totalrecordsperquery']
      assertions.verify_two_values_are_equal(first_value=len(json_response['records']),
                                             second_value=expected_length)
```

#### Assertions
```python
def verify_status_code(resp, status_code=200):
    """
    Function to verify http status code
    :param resp:
    :type resp:
    :param status_code:
    :type status_code:
    :return:
    :rtype:
    """
    assert resp.status_code == status_code, "Response status code should be equal to expected status code"
```


## CI

The project is integrated with Gitlab CI pipeline.

Any push to the repository, triggers the build and the API tests are executed. CI configuration is controlled by
.gitlab-ci.yml file at the root of this project. This is the recommended practice by Gitlab.

The CI for this project has three stages:
- build
- test
- deploy

Currently, build and deploy are **dummy** stages where we just echo on the console.

API tests are run in the "test" job.

## Future

- Optimisations for making project more generic and scalable
- Adding support for new API endpoints

## References

- [Github Public APIs](https://github.com/public-apis/public-apis#art--design)
- [Harvard Museum API documentation](https://github.com/harvardartmuseums/api-docs)
- [This Project's Wiki](https://gitlab.com/bhardwaj.rajat18/auto-api-one/-/wikis/Wiki)




import pytest
from api.request_helper import RequestHelper
import assertions
from settings import constants


class TestResourcePerson:
    """Class to test resource type, Person for Harvard Museums API"""

    # Create object for request util to make payload and call api
    request_util = RequestHelper()

    @pytest.mark.sanity
    def test_person_records_length(self):
        """
        Test to validate the records length is same as the one returned by totalrecordsperquery attribute in response
        Steps:
        1. Call Harvard Museums Person API with query "culture:Dutch" which returns all records
           of people associated with museum coming from Dutch culture
        2. Verify response has some basic attributes: info, totalrecordsperquery and records
        3. Check expected length from totalrecordsperquery attribute
        4. Verify that this is equal to actual length of list of records in response

        Expectation:
        The length of list of records returned should be same as specified in totalrecordsperquery
        """
        json_response = self.request_util.api_get_query(resource_type=constants.RESOURCE_TYPE_PERSON,
                                                        query="culture:Dutch")
        assertions.verify_response_has_attributes(response=json_response, expected_attributes=['info',
                                                                                               'totalrecordsperquery',
                                                                                               'records'])
        expected_length = json_response['info']['totalrecordsperquery']

        # Assert that number of records is equal to what is returned in "totalrecordsperquery"
        assertions.verify_two_values_are_equal(first_value=len(json_response['records']),
                                               second_value=expected_length)

    @pytest.mark.sanity
    def test_person_culture_matches(self):
        """
        Test to validate each record has the same culture as requested by the caller (Dutch in this case)
        Steps:
        1. Call Harvard Museums Person API with query "culture:Dutch" which returns all records
           of people associated with museum coming from Dutch culture
        2. Verify response has some basic attributes: info, totalrecordsperquery and records
        3. Traverse all records and validate if the "culture" key has "Dutch" as the value

        Expectation:
        All records should have "Dutch" as the "culture"
        """

        json_response = self.request_util.api_get_query(resource_type=constants.RESOURCE_TYPE_PERSON,
                                                        query="culture:{0}".format(constants.CULTURE_DUTCH))
        assertions.verify_response_has_attributes(response=json_response, expected_attributes=['info',
                                                                                               'totalrecordsperquery',
                                                                                               'records'])
        expected_culture = constants.CULTURE_DUTCH

        # Assert that the value of 'culture' in each record is "Dutch"
        assertions.verify_two_values_are_equal_in_list_of_objects(list_of_objs=json_response['records'],
                                                                  key='culture', expected_value=expected_culture)

    @pytest.mark.negative
    def test_person_size_greater_than_max(self):
        """
        Test to validate that the response returns less than or equal to the default max records (100)
        per page if size specified is more than max allowed size where size defines the number of records returned per
        page.
        Steps:
        1. Call Harvard Museums Person API with query "gender:female" which returns all records
           of people associated with museum coming from Dutch culture
        2. Verify response has some basic attributes: info, totalrecordsperquery and records
        3. Validate if the "totalrecordsperquery" key has 100 or less than 100 as the value

        Expectation:
        The number of records returned should be less than or equal to 100
        """

        json_response = self.request_util.api_get_query(resource_type=constants.RESOURCE_TYPE_PERSON,
                                                        query="gender:{0}".format(constants.GENDER_FEMALE), size=200)

        # Assert that 'totalrecordsperquery' returns maximum supported number
        assertions.verify_two_values_are_less_or_greater(first_value=json_response['info']['totalrecordsperquery'],
                                                         second_value=constants.MAX_NUM_RECORDS_SUPPORTED)

    @pytest.mark.negative
    def test_person_size_negative(self):
        """
        Test to validate API behaviour when records per page (size) is specified less than 0
        Steps:
        1. Call Harvard Museums Person API with two filter query "culture:Dutch AND gender:female"
           which returns all records of all females associated with museum coming from Dutch culture
        2. Verify response has some basic attributes: info, totalrecordsperquery and records
        3. Verify totalrecordsperquery has 10 as the value because that's the default value in case of invalid size
        4. Verify the number of pages should not be negative

        Expectation:
        API documentation mentions the size and page to be 0-9+
        1. Response should specify the correct default records per page and ignore the invalid input
        2. The number of pages in "pages" should be same as with default size

        Bug:
        Both expectations 1 and 2 are not met

        """
        query_string = "culture:{0} AND gender:{1}".format(constants.CULTURE_DUTCH, constants.GENDER_FEMALE)

        json_response = self.request_util.api_get_query(resource_type=constants.RESOURCE_TYPE_PERSON,
                                                        query=query_string, size=-1)
        assertions.verify_response_has_attributes(response=json_response, expected_attributes=['info',
                                                                                               'totalrecordsperquery',
                                                                                               'records'])
        # TODO: Uncomment assertions to reproduce bug

        # Assert that the number of records and the value of 'totalrecordsperquery' match
        # assertions.verify_two_values_are_equal(first_value=len(json_response['records']),
        #                                             second_value=json_response['info']['totalrecordsperquery'])
        # Assert that 'pages' are not negative
        # assertions.verify_two_values_are_less_or_greater(first_value=json_response['info']['pages'],
        #                                             second_value=0)

    @pytest.mark.regression
    def test_person_multiple_filters_and_sort(self):
        """
        Test to validate multiple filters and sort
        Steps:
        1. Call Harvard Museums Person API with filter query "culture:Dutch AND gender:male", with only data fields,
           gender,culture,displayname, size as 5, sort by id and sortorder as descending
        2. Verify response has some basic attributes: info, totalrecordsperquery and records
        3. Verify all the ids in the list of records returned are actually in descending order

        Expectation:
        API documentation mentions the size and page to be 0-9+
        1. Response should return correct data based on filters
        2. Sort order should be correct
        """
        query_string = "culture:{0} AND gender:{1}".format(constants.CULTURE_DUTCH, constants.GENDER_MALE)
        fields = "gender,culture,displayname"
        json_response = self.request_util.api_get_query(resource_type=constants.RESOURCE_TYPE_PERSON,
                                                        query=query_string,
                                                        fields=fields, size=5, sort="id",
                                                        sortorder="desc")
        assertions.verify_response_has_attributes(response=json_response, expected_attributes=['info',
                                                                                               'totalrecordsperquery',
                                                                                               'records'])
        # Assert that records only has fields requested
        assertions.verify_list_only_has_target_items(list_of_objs=json_response['records'],
                                                     target_items=["gender", "culture", "displayname", "id"])
        # Assert that the value of 'culture' in each record is "Dutch"
        assertions.verify_two_values_are_equal_in_list_of_objects(list_of_objs=json_response['records'],
                                                                  key='culture', expected_value=constants.CULTURE_DUTCH)
        # Assert that the value of 'gender' in each record is "male"
        assertions.verify_two_values_are_equal_in_list_of_objects(list_of_objs=json_response['records'],
                                                                  key='gender', expected_value=constants.GENDER_MALE)
        # Assert that the records are sorted in descending order by 'id'
        assertions.verify_sort_order_by_id(list_of_objs=json_response['records'], sort_order="desc")

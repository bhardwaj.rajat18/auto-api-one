import pytest
from settings.common_settings import CommonSettings
from settings.logger import LoggerSettings
from utils.utility import Utility

util = Utility()


def pytest_addoption(parser):
    parser.addoption("--env", action="store", dest="env", help="Test Environment Ex: core")


def pytest_configure(config):
    CommonSettings.ENV = util.get_config_from_yaml(config.getoption("env"))
    LoggerSettings.LOGGER = util.setup_logger()


@pytest.fixture(scope='class', autouse=True)
def before_class(request):
    """
    Configure logger before the test class is executed
    :param request:
    :return:
    """
    request.cls.logger = LoggerSettings.LOGGER


@pytest.fixture(autouse=True)
def before_test(request):
    """
    :param request:
    :return:
    """
    # Log test function name
    request.cls.logger.info("Executing test case %s ..." % request.function.__name__)

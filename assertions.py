from settings.logger import LoggerSettings

logger = LoggerSettings.LOGGER


def verify_status_code(resp, status_code=200):
    """
    Function to verify http status code
    :param resp:
    :type resp:
    :param status_code:
    :type status_code:
    :return:
    :rtype:
    """
    assert resp.status_code == status_code, "Response status code should be equal to expected status code"


def verify_response_has_attributes(response, expected_attributes):
    """
    Generic assertion to check if a response json has the given attributes/fields
    :param expected_attributes: list of expected fields to be returned in response
    :type expected_attributes:
    :param response: json response received
    :param resp_should_have_items_list: list of fields expected in a response
    :return:
    """
    for key in expected_attributes:
        # Check if the key (attribute) is present in the outer, main json
        if key in response.keys():
            continue
        # If the key (attribute) is not present in outer json, look for a nested json
        elif key not in response.keys():
            for value in response.values():
                if isinstance(value, dict):
                    nested_json = value
                    # Check if key (attribute) is present in the nested json
                    if key in nested_json.keys():
                        continue
                    else:
                        logger.error("Expected '{0}' attribute in response".format(key))
                        raise AssertionError("Expected '{0}' attribute in response".format(key))
        else:
            logger.error("Expected '{0}' attribute in response".format(key))
            raise AssertionError("Expected '{0}' attribute in response".format(key))


def verify_two_values_are_equal(first_value, second_value, equality=True):
    """
    Function to verify if two values are equal or not
    :param first_value:
    :type first_value:
    :param second_value:
    :type second_value:
    :param equality: Asserts inequality if False, equality otherwise
    :type equality: bool
    :return:
    :rtype:
    """
    if equality:
        assert first_value == second_value, "Expected {0}, but actual value is {1}".format(first_value,
                                                                                           second_value)
    else:
        assert first_value != second_value, "Expected {0}, but actual value is {1}".format(first_value,
                                                                                           second_value)


def verify_two_values_are_less_or_greater(first_value, second_value, gt=True):
    """
    Function to verify if first value is greater than or lesser than the second one
    :param first_value:
    :type first_value:
    :param second_value:
    :type second_value:
    :param gt: "Greater than" if True else Less than
    :type gt:
    :return:
    :rtype:
    """
    if gt:
        assert first_value >= second_value, "Expected {0}, but actual value is {1}".format(first_value,
                                                                                           second_value)
    else:
        assert first_value <= second_value, "Expected {0}, but actual value is {1}".format(first_value,
                                                                                           second_value)


def verify_two_values_are_equal_in_list_of_objects(list_of_objs, key, expected_value):
    """
    Function to traverse list of objects and check equality between attribute's value and expected value
    :param list_of_objs: List of objects
    :type list_of_objs: list
    :param key: key/attribute
    :type key:
    :param expected_value:
    :type expected_value:
    :return:
    :rtype:
    """
    for obj in list_of_objs:
        if key in obj.keys():
            assert obj[key] == expected_value
        else:
            logger.error("Expected key: {0} to be in the list of objects".format(key))
            raise AssertionError("Expected key: {0} to be in the list of objects".format(key))


def verify_sort_order_by_id(list_of_objs, sort_order="asc"):
    """
    Function to validate sort order, if the records are sorted by id
    :param list_of_objs: list of target records
    :type list_of_objs: list
    :param sort_order: asc (ascending) or desc (descending)
    :type sort_order: str
    :return:
    :rtype:
    """
    correct_sort = False
    if sort_order == "desc":
        if all(list_of_objs[i]['id'] > list_of_objs[i + 1]['id'] for i in range(len(list_of_objs) - 1)):
            correct_sort = True
    elif sort_order == "asc":
        if all(list_of_objs[i]['id'] < list_of_objs[i + 1]['id'] for i in range(len(list_of_objs) - 1)):
            correct_sort = True
    else:
        raise Exception("Only handling desc or asc sort order")
    assert correct_sort, "The list of records are not sorted in {0} order".format(sort_order)


def verify_list_only_has_target_items(list_of_objs, target_items):
    """
    Function to check if the list of records only has the items specified in target items
    For now, compares first object's keys with target keys assuming all records will be uniform
    :param list_of_objs:
    :type list_of_objs:
    :param target_items:
    :type target_items: list
    :return:
    :rtype:
    """
    assert all(item in list_of_objs[0].keys() for item in target_items)

import yaml
import logging
import os
from settings.logger import LoggerSettings


class Utility:
    """Class for any kind of utility/helper methods that can be reused throughout the project"""

    logger_settings = LoggerSettings()
    dir_path = os.path.dirname(__file__)

    def get_config_from_yaml(self, filename):
        """
        Function to get complete configuration as a dict from yaml
        :param filename: yaml config file to read
        :type filename: str
        :return: config object
        :rtype: dict
        """
        try:
            with open(self.dir_path + "/../config/" + filename + ".yaml", "r") as f:
                config = yaml.safe_load(f)
                return config
        except FileNotFoundError:
            print("\nPlease make sure you pass the correct environment and the environment yaml file exists"
                  " in config directory\n")
            raise

    def setup_logger(self, log_file_name=LoggerSettings.LOG_FILE_NAME):
        """
        Function to setup basic configurations for logger
        :param log_file_name: Log file name where logs will be written
        :type log_file_name: str
        :return: logger object
        :rtype:
        """
        logger = logging.getLogger(LoggerSettings.LOGGER_NAME)
        logger.setLevel(logging.DEBUG)
        fh = logging.FileHandler(filename=self.dir_path + self.logger_settings.LOGGER_PATH + log_file_name, mode='w')
        fh.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s %(levelname)s - %(name)s %(filename)s:%(lineno)d %(message)s',
                                      datefmt='%a, %d %b %Y %H:%M:%S')
        fh.setFormatter(formatter)

        logger.addHandler(fh)
        return logger
